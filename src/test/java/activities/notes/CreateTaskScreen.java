package activities.notes;

import controlAppium.Button;
import controlAppium.TextBox;
import org.openqa.selenium.By;

public class CreateTaskScreen {
    public TextBox titleTextBox = new TextBox(By.id("com.miui.notes:id/note_title"));
    public TextBox descriptionTextBox = new TextBox(By.id("com.miui.notes:id/rich_editor"));
    public Button saveButton= new Button(By.id("com.miui.notes:id/done"));
    public Button backButton= new Button(By.id("com.miui.notes:id/home"));
    public CreateTaskScreen(){}
}
