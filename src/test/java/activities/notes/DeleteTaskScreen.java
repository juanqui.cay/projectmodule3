package activities.notes;

import controlAppium.Button;
import org.openqa.selenium.By;

public class DeleteTaskScreen {

    public Button moreButton = new Button(By.id("com.miui.notes:id/more"));
    public Button deleteButton= new Button(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[5]"));
    public Button deleteConfirmationButton= new Button(By.id("android:id/button1"));
    public DeleteTaskScreen(){}
}
