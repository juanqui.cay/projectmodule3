package activities.notes;

import controlAppium.Button;
import controlAppium.Label;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class MainScreen {
    public Button jumpButton = new Button(By.id("android:id/button2"));
    public Button addTaskButton = new Button(By.id("com.miui.notes:id/content_add"));
    public Label nameTaskLabel = new Label(By.id("com.miui.notes:id/preview"));
    public Label selectTask = new Label(By.id("com.miui.notes:id/preview"));
    public MainScreen(){}

    public boolean isItemDisplayed(){
        try{
            selectTask.getText();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
