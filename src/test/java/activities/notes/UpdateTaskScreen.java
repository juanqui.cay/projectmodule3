package activities.notes;

import controlAppium.Button;
import controlAppium.TextBox;
import org.openqa.selenium.By;

public class UpdateTaskScreen {
    public TextBox updateTitleTextBox = new TextBox(By.id("com.miui.notes:id/note_title"));
    public TextBox updateDescriptionTextBox = new TextBox(By.id("com.miui.notes:id/rich_editor"));
    public Button saveButton= new Button(By.id("com.miui.notes:id/done"));
    public UpdateTaskScreen(){}
}
