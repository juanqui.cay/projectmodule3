package basicTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BasicAppium {

    private AppiumDriver driver;
    // setup - initialize
    @Before
    public void setup() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","sho");
        capabilities.setCapability("platformVersion","9");
        capabilities.setCapability("appPackage","com.vrproductiveapps.whendo");
        capabilities.setCapability("appActivity",".ui.HomeActivity");
        capabilities.setCapability("platformName","android");

        driver = new AndroidDriver( new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        //implicit wait
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

    }

    // cleanup - teardown
    @After
    public void cleanup() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    @Test
    public void createTaskWhenDo(){

        // Click +
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/fab")).click();
        // Set Title
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).sendKeys("NUEVA TAREA");
        // Set Notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).sendKeys("esta es una descripcion de la nueva tarea");
        // Click Save
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/saveItem")).click();
        // Verificacion

        String expectedResult="NUEVA TAREA";
        String actualResult=driver.findElement(By.id("com.vrproductiveapps.whendo:id/home_list_item_text")).getText();

        Assert.assertEquals("ERROR tarea no fue creado!",expectedResult,actualResult);
    }

    @Test
    public void UpdateTaskWhenDo(){
/*
        //Crearemos primero la tarea para poder actualizarlo
        // Click +
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/fab")).click();
        // Set Title
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).sendKeys("NUEVA TAREA");
        // Set Notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).sendKeys("esta es una descripcion de la nueva tarea");
        // Click Save
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/saveItem")).click();
        // Verificacion*/

        // Click en tarea creada
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/home_list_item_text")).click();
        // Eliminar datos de campo
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).clear();
        // actualizar nuevo titulos
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).sendKeys("Tarea actualizada");
        // Eliminar datos de campo notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).clear();
        // Set Notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).sendKeys("Estas notas tambien se actualizaron");
        // Click Save
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/saveItem")).click();
        // Verificacion

        String expectedResult="Tarea actualizada";
        String actualResult=driver.findElement(By.id("com.vrproductiveapps.whendo:id/home_list_item_text")).getText();
        Assert.assertEquals("ERROR tarea no fue creado!",expectedResult,actualResult);
    }

    @Test
    public void DeleteTaskWhenDo(){
/*
        //Crearemos primero la tarea para poder actualizarlo
        // Click +
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/fab")).click();
        // Set Title
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).sendKeys("NUEVA TAREA");
        // Set Notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).sendKeys("esta es una descripcion de la nueva tarea");
        // Click Save
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/saveItem")).click();
        // Verificacion

        // Click en tarea creada
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/home_list_item_text")).click();
        // Eliminar datos de campo
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).clear();
        // actualizar nuevo titulos
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextTitle")).sendKeys("Tarea actualizada");
        // Eliminar datos de campo notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).clear();
        // Set Notas
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/noteTextNotes")).sendKeys("Estas notas tambien se actualizaron");
        // Click Save
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/saveItem")).click();
        // Verificacion*/


        // Click en tarea creada
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/home_list_item_text")).click();
        // Eliminar Tarea
        driver.findElement(By.id("com.vrproductiveapps.whendo:id/deleteItem")).click();
        // Confirmar eliminacion
        driver.findElement(By.id("android:id/button1")).click();

    }


}
