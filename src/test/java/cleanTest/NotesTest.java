package cleanTest;

import activities.notes.CreateTaskScreen;
import activities.notes.DeleteTaskScreen;
import activities.notes.MainScreen;
import activities.notes.UpdateTaskScreen;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import session.Session;

import java.net.MalformedURLException;

public class NotesTest {

    private MainScreen mainScreen= new MainScreen();
    private CreateTaskScreen createTaskScreen= new CreateTaskScreen();
    private UpdateTaskScreen updateTaskScreen= new UpdateTaskScreen();
    private DeleteTaskScreen deleteTaskScreen= new DeleteTaskScreen();

    @Test
    public void verifyCreateTask() throws MalformedURLException {
        String title="Descripción de tarea MIUI XIAOMI";
        mainScreen.jumpButton.click();
        mainScreen.addTaskButton.click();
        createTaskScreen.titleTextBox.setValue(title);
        createTaskScreen.descriptionTextBox.setValue("Descripción de tarea MIUI XIAOMI");
        createTaskScreen.saveButton.click();
        createTaskScreen.backButton.click();
        Assert.assertTrue("ERROR",mainScreen.nameTaskLabel.getText().contains(title));
    }

    @Test
    public void verifyUpdateTask() throws MalformedURLException {
        verifyCreateTask();
        String title="Titulo Actualizado 2";
        String description="Caja actualizada 2";
        mainScreen.selectTask.click();
        updateTaskScreen.updateTitleTextBox.setValue(title);
        updateTaskScreen.updateDescriptionTextBox.setValue(description);
        createTaskScreen.backButton.click();
        Assert.assertTrue("ERROR",mainScreen.nameTaskLabel.getText().contains(description));
    }

    @Test
    public void verifyDeleteTask() throws MalformedURLException {
        verifyCreateTask();
        String title="Nuevo titulo para borrar";
        mainScreen.selectTask.click();
        deleteTaskScreen.moreButton.click();

        deleteTaskScreen.deleteButton.click();
        deleteTaskScreen.deleteConfirmationButton.click();
    }

    @After
    public void close() throws MalformedURLException {
        Session.getInstance().closeSession();
    }
}
GRACIAS!!!!!!